const stream = require('./stream');
const NetworkNode = stream.NetworkNode;
const Benchmark = require('benchmark');

// TODO : Optimize path seek.
const a = new NetworkNode('a');
const b = new NetworkNode('b',true);
const c = new NetworkNode('c');
const d = new NetworkNode('d',true);

a.pipe(b);
b.pipe(c);
b.pipe(d);

let suite = new Benchmark.Suite;
 
// add tests 
suite.add('forward#Event', function() {
    a.forward({
        to: "c",
        content: "hello world!"
    });
})
.on('cycle', function(event) {
  console.log(String(event.target));
})
.on('complete', function() {
  console.log('done');
})
.run({ 'async': true });